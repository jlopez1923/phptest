<p>
<img width="500" src="https://napoleon.chat/images/logo.png">
</p>

# PhpTest NapoleoN Systems

# Background
Tu misión es crear un sistema de chat simple, piensa en tu experiencia de comunicacion diaria en redes sociales. 
- En caso de duda, opta por la solución más sencilla. 
- Concéntrate en construir la lógica del dominio, no te molestes en crear ningún tipo de interfaz de usuario hasta que se te solicite explícitamente.
- El ejercicio evoluciona como una secuencia de iteraciones, intenta completar cada iteración antes de leer la siguiente.

# What to do

## Iteración 1: Listar contactos
Puedes seguir esta historia de usuario:

```gherkin
Feature: List your contacts and groups
    As a client of NNchat application
    I want to be able to list all my contacts
    In order to chat whit them

    Scenario: An existing client list all your contacts and groups
        Given an existing client with id “francisco” with 8 contacts in his list and 2 groups
        When he selects a contact or group from his list
        Then it will show an interface to be able to chat
```

## Iteración 2: Mostrar la burbuja de mensajes pendientes
Debes mostrar la burbuja con el numero de mensajes pendientes por leer en cada chat, al ingresar al chat debera mostrar desde el inicio los mensajes sin leer.

## Iteración 3: Agregar la funcion de chatear
Puedes seguir esta historia de usuario:

```gherkin
Feature: Chat with a contact or a group
    As a client of NNchat application
    I want to send and receive messages from any contact or group on my list
    In order to write them

    Scenario: An existing client writes messages with a contacts
        Given an existing customer in a conversation with a contact
        When you send and receive messages
        Then change the status of the message
```

## Iteración 4: Actualizar estado del mensaje
Agrega el escenario en la función de actualizar estado de mensaje (enviado, entregado, leido o perdido). En caso de perder un time out en el proceso por conexion hacer el reenvio automático.

# Retrospectivo

¿Estás al día con los requisitos? ¿Alguna iteración ha sido un gran desafío?
¿Te sientes bien con tu diseño? ¿Es escalable y se adapta fácilmente a los nuevos requisitos que surgen en futuras iteraciones?
¿Está todo probado? ¿Confías en tus pruebas?

## Iteración 5: agregue una GUI de Laravel
Expon las funciones existentes (comunicacion entre emisor(es) y receptor(es)) a través de una aplicación web muy simple. No se necesitan animaciones ni diseños de interfaz de usuario elaborados.

# Consideraciones
- Propon la arquitectura e implementala
- Has buen uso de las principios SOLID y patrones de diseño
- Realiza el modelo de la base de datos relacional e implementalo

# Obligatorio
- Usa como herramientas de desarrollo PHP, LARAVEL Y POSTGRESS
- Usa sockets
- Usa buenas practicas de git por cada iteración 




